﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace MVC_Aula.Model
{
    [Serializable]
    [BsonIgnoreExtraElements]
    public class Components
    {
        [BsonId]
        public ObjectId _id { get; set; }

        [BsonElement("processId")]
        public string processId { get; set; }

        [BsonElement("type")]
        public string type { get; set; }

        [BsonElement("description")]
        public string description { get; set; }

        [BsonElement("bridge")]
        public string bridge { get; set; }
    }
}
