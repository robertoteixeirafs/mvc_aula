﻿using MongoDB.Driver;
using System;

namespace MVC_Aula.Controller
{

    public class MongoConnection
    {

        public static IMongoDatabase database;
        public static MongoClient client;

        public static void Connect(string ip, int port, string dbName)
        {
            client = new MongoClient(
                new MongoClientSettings
                {
                    Server = new MongoServerAddress(ip, port),
                    ServerSelectionTimeout = TimeSpan.FromSeconds(3)
                });
            database = client.GetDatabase(dbName);
        }

    }
}
