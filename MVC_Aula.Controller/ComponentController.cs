﻿using MongoDB.Driver;
using MVC_Aula.Model;
using System.Collections.Generic;
using System.Linq;

namespace MVC_Aula.Controller
{
    public class ComponentController
    {
        public ComponentController()
        {
            MongoConnection.Connect("10.145.253.24", 30017, "smh");
        }

        public List<Components> ReturnAllComponents()
        {
            IMongoCollection<Components> components = MongoConnection.database.GetCollection<Components>("components");
            var documents = components.Find(FilterDefinition<Components>.Empty).ToList();
            return documents;
        }
    }
}
