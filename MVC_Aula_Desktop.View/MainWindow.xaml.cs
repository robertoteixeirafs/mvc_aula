﻿using MVC_Aula.Controller;
using MVC_Aula.Model;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace MVC_Aula_Desktop.View
{

    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void LoadComponentsClick(object sender, RoutedEventArgs e)
        {
            try
            {
                ComponentController componentController = new ComponentController();
                List<Components> components = componentController.ReturnAllComponents();

                if (components.Count > 0)
                {
                    Binding bind = new Binding();
                    lvComponents.DataContext = components;
                    lvComponents.SetBinding(ListView.ItemsSourceProperty, bind);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
