﻿using MVC_Aula.Controller;
using MVC_Aula.Model;
using System.Collections.Generic;
using System.Web.Services;

namespace MVC_Aula_Web.View.Webservices
{

    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]    
    [System.Web.Script.Services.ScriptService]
    public class WebService : System.Web.Services.WebService
    {

        [WebMethod]        
        public List<Components> LoadComponents()
        {
            ComponentController componentController = new ComponentController();
            return componentController.ReturnAllComponents();
        }
        
    }
}
